﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace FilmKitapBilgisi.Models
{
    public class Uye
    {
        //Eğer modelimize ilave öznitelikler katarak (DataAnnotations) katarak biraz daha detaylı bir biçimlendirme yapacak olursak, Validation işlemleri de bizim burada vereceğimiz ilave bilgilere paralel olarak şekillenecektir.

        //StringLength => MinLength ve MaxLength'i kapsar. Bunu kullanın..

        [Required(ErrorMessage="Ad alanı zorunludur")]
        [Display(Name="İsim:")]
        [StringLength(30, ErrorMessage="Ad alanı minimum 3, maksimum 30 karakter olmalıdır.", MinimumLength=3)]
        public string Ad { get; set; }

        [Required(ErrorMessage = "Soyad alanı zorunludur")]
        [Display(Name = "Soyadı:")]
        public string Soyad { get; set; }

        [Display(Name = "Doğum Tarihi:")]
        [DataType(DataType.Date)]        
        //[Range()] => Eğer tarih için belirli bir aralık kontrol etmek istiyorsak, Range kullanacağız..
        public DateTime DogumTarihi { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Eposta alanı zorunludur")]
        [EmailAddress(ErrorMessage="Girmiş olduğunuz bilgi eposta formatına uygun değildir. Kontrol ederek yeniden deneyiniz.")]
        public string Eposta { get; set; }

        [Display(Name = "Şifre:")]
        [Required(ErrorMessage = "Şifre alanı zorunludur")]
        [DataType(DataType.Password)]
        [Compare("SifreTekrar", ErrorMessage="Şifreler uyuşmuyor, yeniden deneyin.")]
        public string Sifre { get; set; }

        [Display(Name = "Şifre Tekrar:")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Şifre Tekrar alanı zorunludur")]
        public string SifreTekrar { get; set; }
    }
    
}