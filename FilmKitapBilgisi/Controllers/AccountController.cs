﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.Data;
using System.Web.Security;
using FilmKitapBilgisi;
using WebMatrix.WebData;
using FilmKitapBilgisi.Models;

namespace FilmKitapBilgisi.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        KitapFilmWebEntities db = new KitapFilmWebEntities();
        public ActionResult UyeKayit()
        {
            return View();
        }

        [HttpPost]
        public ActionResult UyeKayit(UyeBilgi uye)
        {
            //if (ModelState.IsValid)
            //{
            //    try
            //    {

            //        if (!Roles.RoleExists("uye"))
            //        {
            //            Roles.CreateRole("uye");
            //        }
            //        WebSecurity.CreateUserAndAccount(uye.uyeEmail, uye.uyeSifre, new { uye.uyeAdi, uye.uyeSoyadi, uye.uyeEmail, uye.uyeSifreTekrar });
            //        //Yeni kayıt olan bir kullanıcı her halukarda user rolünde olmalı, gerekirse bunun rolü farklı bir kontrol paneli içerisinde değiştirilebilir.
            //        Roles.AddUserToRole(uye.uyeEmail, "uye");
            //        return RedirectToAction("Index", "Home");
            //    }
            //    catch (Exception ex)
            //    {
            //        return RedirectToAction("Error", "Account", new { errorMessage = ex.Message });
            //    }
            //}
            //else
            //{
            //    return RedirectToAction("Index", "Home");
            //}

            db.UyeBilgi.Add(uye);
            db.SaveChanges();
            return View();

            
        }




        public ActionResult GirisYap()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GirisYap(UyeBilgi uye)
        {
            //if (WebSecurity.Login(uye.uyeEmail, uye.uyeSifre))
            //{
                return RedirectToAction("Index", "Home");
            //}
            //else
            //{
            //    return RedirectToAction("Error", "Account", new { errorMessage = "Kullanıcı Adı veya Şifreniz Hatalı. Lütfen Kontrol Edip Tekrar Deneyiniz." });
            //}
        }



        


        public ActionResult KullaniciSayfasi()
        {
            UyeBilgi secilenKullanici = db.UyeBilgi.Find(WebSecurity.CurrentUserId);
            return View(secilenKullanici);
        }

        public ActionResult Cikis()
        {
            WebSecurity.Logout();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Hata(string errorMessage)
        {
            ViewBag.YakalananHata = errorMessage;
            return View();
        }
    }
}
