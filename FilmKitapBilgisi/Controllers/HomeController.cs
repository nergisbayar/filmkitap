﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace FilmKitapBilgisi.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        KitapFilmWebEntities db = new KitapFilmWebEntities(); 
        public ActionResult Index()
        {
            int saat = DateTime.Now.Hour;
            ViewBag.Mesaj = saat < 12 ? "Günaydın" : "Tünaydın";

            
            return View();
        }

        public ActionResult Kitap()
        {
            var model = db.KitapBilgi.ToList();
            return View(model);
        }

        public ActionResult KitapDetay(int id)
        {
            var model = db.KitapBilgi.Find(id);
            return View(model);
        }
        public ActionResult Film()
        {
            var model = db.FilmBilgi.ToList();
            return View(model);
        }

        public ActionResult FilmDetay(int id)
        {
            var model = db.FilmBilgi.Find(id);
            return View(model);
        }

    }
}
